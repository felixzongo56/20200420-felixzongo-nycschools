package com.example.nycschools;

import android.content.Context;

import com.example.nycschools.model.School;
import com.example.nycschools.model.SchoolsSATScores;
import com.example.nycschools.viewmodel.ItemSchoolsViewModel;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;

import static org.junit.Assert.assertEquals;

@RunWith(MockitoJUnitRunner.class)
public class ItemSchoolViewModelTest {

    private static final String SHCOOL_NAME = "New York School";
    private static final String SCHOOL_STUDENTS = "600";
    private static final String SCHOOL_PHONE = "2120648977";
    private static final String SCHOOL_EMAIL = "school.nyc@nyc.gove";
    private static final String SCHOOL_WEBSITE = "school.nyc.gov";
    private static final String SHHOOL_ADDRESS = " 1 City Hall Park, New York, NY 10007";

    @Mock
    private Context mockContext;

    @Before
    public void setUp () {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void shouldGetSchoolName () throws Exception {
        School school = new School();
        SchoolsSATScores schoolsSATScores = new SchoolsSATScores();

        school.setSchoolName(SHCOOL_NAME);
        ItemSchoolsViewModel itemSchoolsViewModel = new ItemSchoolsViewModel(school, schoolsSATScores,mockContext);
        assertEquals(school.getSchoolName(),itemSchoolsViewModel.getSchoolName());
    }


}
