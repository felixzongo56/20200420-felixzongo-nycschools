package com.example.nycschools;

import android.content.Context;

import androidx.multidex.MultiDexApplication;

import com.example.nycschools.data.SchoolsFactory;
import com.example.nycschools.data.SchoolsService;

import io.reactivex.Scheduler;
import io.reactivex.schedulers.Schedulers;

public class SchoolsApplication extends MultiDexApplication {

    private SchoolsService schoolsService;
    private Scheduler scheduler;

    private static SchoolsApplication get(Context context) {
        return (SchoolsApplication) context.getApplicationContext();
    }

    public static SchoolsApplication create(Context context) {
        return SchoolsApplication.get(context);
    }

    public SchoolsService getNYCSchoolsService() {
        if (schoolsService == null) {
            schoolsService = SchoolsFactory.create();
        }
        return schoolsService;
    }

    public Scheduler subscribeScheduler() {
        if (scheduler == null) {
            scheduler = Schedulers.io();
        }
        return scheduler;
    }
}
