package com.example.nycschools.viewmodel;

import com.example.nycschools.model.School;
import com.example.nycschools.model.SchoolsSATScores;

public class SchoolsDetailViewModel {

    private static String TEL = "Tel: ";
    private static String EMAIL = "Email: ";
    private static String SAT_TEST_TAKER = "number Of SAT Test Takers: ";
    private static String SAT_CRITICAL_READING_AVG_SCORE = "SAT Critical Reading Average Score: ";
    private static String SAT_MATH_AVG_SCORE = "SAT Math_Average Score: ";
    private static String SAT_WRITING_AVG_SCORE = "SAT Writing Average Score: ";
    private static String NA = "N/A";


    private School school;
    private SchoolsSATScores schoolsSATScores;

    public SchoolsDetailViewModel(School schools, SchoolsSATScores schoolsSATScores) {
        this.school = schools;
        this.schoolsSATScores = schoolsSATScores;
    }

    public String getSchoolName() {
        return school.getSchoolName();
    }

    public String getPhoneNumber() {
        return TEL + school.getPhoneNumber();
    }

    public String getSchoolEmail() {
        return EMAIL + school.getSchoolEmail();
    }

    public String getNumOfSatTestTakers() {
        return schoolsSATScores.getNumOfSatTestTakers() != null ? SAT_TEST_TAKER + schoolsSATScores.getNumOfSatTestTakers() : SAT_TEST_TAKER + NA;
    }

    public String getSatCriticalReadingAvgScore() {
        return schoolsSATScores.getSatCriticalReadingAvgScore() != null ? SAT_CRITICAL_READING_AVG_SCORE + schoolsSATScores.getSatCriticalReadingAvgScore() : SAT_CRITICAL_READING_AVG_SCORE + NA;
    }

    public String getSatMathAvgScore() {
        return schoolsSATScores.getSatMathAvgScore() != null ? SAT_MATH_AVG_SCORE + schoolsSATScores.getSatMathAvgScore() : SAT_MATH_AVG_SCORE + NA;
    }

    public String getSatWritingAvgScore() {
        return schoolsSATScores.getSatWritingAvgScore() != null ? SAT_WRITING_AVG_SCORE + schoolsSATScores.getSatWritingAvgScore() : SAT_WRITING_AVG_SCORE + NA;
    }
}
