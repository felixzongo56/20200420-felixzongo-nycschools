package com.example.nycschools.viewmodel;

import android.content.Context;
import android.view.View;

import androidx.databinding.BaseObservable;

import com.example.nycschools.model.School;
import com.example.nycschools.model.SchoolsSATScores;
import com.example.nycschools.view.SchoolsDetailActivity;

public class ItemSchoolsViewModel extends BaseObservable {
    private static String SPACE = " ";
    private static String COMMA = ", ";
    private static String TEL = "Tel: ";
    private static String EMAIL = "Email: ";
    private static String WEBSITE = "Website:";
    private static String TOTAL_STUDENTS = "Number Of Students: ";
    private static String ADDRESS = "Address: ";

    private School school;
    private SchoolsSATScores schoolsSATScores;
    private Context context;

    public ItemSchoolsViewModel(School school, SchoolsSATScores schoolsSATScores, Context context) {
        this.school = school;
        this.schoolsSATScores = schoolsSATScores;
        this.context = context;
    }

    public String getSchoolName() {
        return school.getSchoolName();
    }

    public String getPhoneNumber() {
        return TEL + school.getPhoneNumber();
    }

    public String getSchoolEmail() {
        return EMAIL + school.getSchoolEmail();
    }

    public String getTotalStudents() {
        return TOTAL_STUDENTS + school.getTotalStudents();
    }

    public String getAddress() {
        return ADDRESS + school.getPrimaryAddressLine1() + SPACE + school.getCity() + COMMA + school.getStateCode() + SPACE + school.getZip();
    }

    public String getWebsite() {
        return WEBSITE + school.getWebsite();
    }

    public void onItemClick(View view) {
        context.startActivity(SchoolsDetailActivity.launchDetail(view.getContext(), school, schoolsSATScores));
    }

    public void setSchool(School school) {
        this.school = school;
        notifyChange();
    }
}
