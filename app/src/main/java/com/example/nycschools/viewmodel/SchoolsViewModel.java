package com.example.nycschools.viewmodel;

import android.content.Context;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.databinding.ObservableField;
import androidx.databinding.ObservableInt;

import com.example.nycschools.R;
import com.example.nycschools.SchoolsApplication;
import com.example.nycschools.data.SchoolsFactory;
import com.example.nycschools.data.SchoolsService;
import com.example.nycschools.model.School;
import com.example.nycschools.model.SchoolsSATScores;

import java.util.ArrayList;
import java.util.List;
import java.util.Observable;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;

public class SchoolsViewModel extends Observable {

    public ObservableInt schoolsProgress;
    public ObservableInt schoolsRecycler;
    public ObservableInt schoolsLabel;
    public ObservableField<String> messageLabel;

    private List<School> schoolList;
    private List<SchoolsSATScores> schoolsSATScoresList;
    private Context context;
    private CompositeDisposable compositeDisposable = new CompositeDisposable();

    public SchoolsViewModel(@NonNull Context context) {

        this.context = context;
        this.schoolList = new ArrayList<>();
        this.schoolsSATScoresList = new ArrayList<>();
        schoolsProgress = new ObservableInt(View.GONE);
        schoolsRecycler = new ObservableInt(View.GONE);
        schoolsLabel = new ObservableInt(View.VISIBLE);
        messageLabel = new ObservableField<>(context.getString(R.string.default_loading_schools));
    }

    public void onClickFabLoad(View view) {
        initializeViews();
        fetchSchoolsList();
        fetchSchoolsSATList();
    }

    public void initializeViews() {
        schoolsLabel.set(View.GONE);
        schoolsRecycler.set(View.GONE);
        schoolsProgress.set(View.VISIBLE);
    }

    private void fetchSchoolsList() {

        SchoolsApplication schoolsApplication = SchoolsApplication.create(context);
        SchoolsService schoolsService = schoolsApplication.getNYCSchoolsService();

        Disposable disposable = schoolsService.fetchSchools(SchoolsFactory.SCHOOL_URL)
                .subscribeOn(schoolsApplication.subscribeScheduler())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Consumer<List<School>>() {
                    @Override
                    public void accept(List<School> schoolsResponse) {
                        changeNYCSchoolsDataSet(schoolsResponse);
                        schoolsProgress.set(View.GONE);
                        schoolsLabel.set(View.GONE);
                        schoolsRecycler.set(View.VISIBLE);
                    }
                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(Throwable throwable) {
                        messageLabel.set(context.getString(R.string.error_loading_schools));
                        schoolsProgress.set(View.GONE);
                        schoolsLabel.set(View.VISIBLE);
                        schoolsRecycler.set(View.GONE);
                        throwable.printStackTrace();
                    }
                });

        compositeDisposable.add(disposable);
    }

    private void fetchSchoolsSATList() {

        SchoolsApplication schoolsApplication = SchoolsApplication.create(context);
        SchoolsService schoolsService = schoolsApplication.getNYCSchoolsService();

        Disposable disposable = schoolsService.fetchSchoolsSAT(SchoolsFactory.SCHOOLS_SAT_URL)
                .subscribeOn(schoolsApplication.subscribeScheduler())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Consumer<List<SchoolsSATScores>>() {
                    @Override
                    public void accept(List<SchoolsSATScores> schoolsResponse) {
                        changeNYCSchoolsSATDataSet(schoolsResponse);
                        schoolsProgress.set(View.GONE);
                        schoolsLabel.set(View.GONE);
                        schoolsRecycler.set(View.VISIBLE);
                    }
                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(Throwable throwable) {
                        messageLabel.set(context.getString(R.string.error_loading_schools));
                        schoolsProgress.set(View.GONE);
                        schoolsLabel.set(View.VISIBLE);
                        schoolsRecycler.set(View.GONE);
                        throwable.printStackTrace();
                    }
                });

        compositeDisposable.add(disposable);
    }

    private void changeNYCSchoolsSATDataSet(List<SchoolsSATScores> schoolsResponse) {
        schoolsSATScoresList.addAll(schoolsResponse);
        setChanged();
        notifyObservers();
    }

    private void changeNYCSchoolsDataSet(List<School> schools) {
        schoolList.addAll(schools);
        setChanged();
        notifyObservers();
    }

    public List<School> getSchoolsList() {
        return schoolList;
    }

    public List<SchoolsSATScores> getSchoolsSATList() {
        return schoolsSATScoresList;
    }

    private void unSubscribeFromObservable() {
        if (compositeDisposable != null && !compositeDisposable.isDisposed()) {
            compositeDisposable.dispose();
        }
    }

    public void reset() {
        unSubscribeFromObservable();
        compositeDisposable = null;
        context = null;
    }
}
