package com.example.nycschools.data;

import com.example.nycschools.model.School;
import com.example.nycschools.model.SchoolsSATScores;

import java.util.List;

import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.Url;

public interface SchoolsService {

  @GET Observable<List<School>> fetchSchools(@Url String url);
  @GET Observable<List<SchoolsSATScores>> fetchSchoolsSAT(@Url String url);

}
