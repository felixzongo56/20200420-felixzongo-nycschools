package com.example.nycschools.data;

import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

public class SchoolsFactory {

  public final static String BASE_URL = "https://data.cityofnewyork.us/resource/";
  public final static String SCHOOL_URL = "https://data.cityofnewyork.us/resource/s3k6-pzi2.json";
  public final static String SCHOOLS_SAT_URL = "https://data.cityofnewyork.us/resource/f9bf-2cp4.json";

  public static SchoolsService create() {
    Retrofit retrofit = new Retrofit.Builder().baseUrl(BASE_URL)
        .addConverterFactory(GsonConverterFactory.create())
        .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
        .build();
    return retrofit.create(SchoolsService.class);
  }
}
