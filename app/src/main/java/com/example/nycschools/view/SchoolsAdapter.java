package com.example.nycschools.view;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;


import com.example.nycschools.R;
import com.example.nycschools.model.School;
import com.example.nycschools.model.SchoolsSATScores;
import com.example.nycschools.viewmodel.ItemSchoolsViewModel;
import com.example.nycschools.databinding.ItemSchoolBinding;

import java.util.Collections;
import java.util.List;


public class SchoolsAdapter extends RecyclerView.Adapter<SchoolsAdapter.SchoolsAdapterViewHolder> {

    private List<School> schoolList;
    private List<SchoolsSATScores> schoolsSATScores;

    SchoolsAdapter() {
        this.schoolList = Collections.emptyList();
        this.schoolsSATScores = Collections.emptyList();
    }

    @NonNull
    @Override
    public SchoolsAdapterViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ItemSchoolBinding itemSchoolBinding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.item_school,
                parent, false);
        return new SchoolsAdapterViewHolder(itemSchoolBinding);
    }

    @Override
    public void onBindViewHolder(SchoolsAdapterViewHolder holder, int position) {
        holder.bindSchools(schoolList.get(position), getSchoolSTAScores(schoolList.get(position)));
    }

    private SchoolsSATScores getSchoolSTAScores(School school) {
        if (schoolsSATScores != null) {
            for (int i = 0; i < schoolsSATScores.size(); i++) {
                if (school.getDbn().matches(schoolsSATScores.get(i).getDbn())) {
                    return (schoolsSATScores.get(i));
                }
            }
        }
        return new SchoolsSATScores();
    }

    @Override
    public int getItemCount() {
        return schoolList.size();
    }

    void setSchoolList(List<School> schoolList) {
        this.schoolList = schoolList;
        notifyDataSetChanged();
    }

    void setSchoolSATList(List<SchoolsSATScores> setSchoolSATList) {
        this.schoolsSATScores = setSchoolSATList;
    }

    static class SchoolsAdapterViewHolder extends RecyclerView.ViewHolder {
        ItemSchoolBinding mItemSchoolsBinding;

        SchoolsAdapterViewHolder(ItemSchoolBinding itemSchoolsBinding) {
            super(itemSchoolsBinding.itemSchool);
            this.mItemSchoolsBinding = itemSchoolsBinding;
        }

        void bindSchools(School school, SchoolsSATScores schoolsSATScores) {
            if (mItemSchoolsBinding.getSchoolsViewModel() == null) {
                mItemSchoolsBinding.setSchoolsViewModel(
                        new ItemSchoolsViewModel(school, schoolsSATScores, itemView.getContext()));
            } else {
                mItemSchoolsBinding.getSchoolsViewModel().setSchool(school);
            }
        }
    }
}
