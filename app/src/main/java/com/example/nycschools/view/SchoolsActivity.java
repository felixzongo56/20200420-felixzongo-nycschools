package com.example.nycschools.view;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.nycschools.R;
import com.example.nycschools.data.SchoolsFactory;
import com.example.nycschools.databinding.SchoolsActivityBinding;
import com.example.nycschools.viewmodel.SchoolsViewModel;

import java.util.Observable;
import java.util.Observer;

public class SchoolsActivity extends AppCompatActivity implements Observer {

    private SchoolsActivityBinding schoolsActivityBinding;
    private SchoolsViewModel schoolsViewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        initDataBinding();
        setSupportActionBar(schoolsActivityBinding.toolbar);
        setupListSchoolsView(schoolsActivityBinding.listSchools);
        setupObserver(schoolsViewModel);
    }

    private void initDataBinding() {
        schoolsActivityBinding = DataBindingUtil.setContentView(this, R.layout.schools_activity);
        schoolsViewModel = new SchoolsViewModel(this);
        schoolsActivityBinding.setMainViewModel(schoolsViewModel);
    }

    private void setupListSchoolsView(RecyclerView listSchools) {
        SchoolsAdapter adapter = new SchoolsAdapter();
        listSchools.setAdapter(adapter);
        listSchools.setLayoutManager(new LinearLayoutManager(this));
    }

    public void setupObserver(SchoolsViewModel observable) {
        observable.addObserver(this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        schoolsViewModel.reset();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.menu_schools) {
            startActivityActionView();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void startActivityActionView() {
        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(SchoolsFactory.BASE_URL)));
    }

    @Override
    public void update(Observable observable, Object data) {
        if (observable instanceof SchoolsViewModel) {
            SchoolsAdapter SchoolsAdapter = (SchoolsAdapter) schoolsActivityBinding.listSchools.getAdapter();
            SchoolsViewModel schoolsViewModel = (SchoolsViewModel) observable;
            SchoolsAdapter.setSchoolList(schoolsViewModel.getSchoolsList());
            SchoolsAdapter.setSchoolSATList(schoolsViewModel.getSchoolsSATList());
        }
    }
}
