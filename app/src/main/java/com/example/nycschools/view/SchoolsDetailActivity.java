package com.example.nycschools.view;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import com.example.nycschools.R;
import com.example.nycschools.databinding.SchoolsDetailActivityBinding;
import com.example.nycschools.model.School;
import com.example.nycschools.model.SchoolsSATScores;
import com.example.nycschools.viewmodel.SchoolsDetailViewModel;

public class SchoolsDetailActivity extends AppCompatActivity {

    private static final String EXTRA_SCHOOL = "EXTRA_SCHOOL";
    private static final String EXTRA_SCHOOL_SAT = "EXTRA_SCHOOL_SAT";

    private SchoolsDetailActivityBinding schoolsDetailActivityBinding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        schoolsDetailActivityBinding =
                DataBindingUtil.setContentView(this, R.layout.schools_detail_activity);
        setSupportActionBar(schoolsDetailActivityBinding.toolbar);
        displayHomeAsUpEnabled();
        getExtrasFromIntent();
    }

    public static Intent launchDetail(Context context, School school, SchoolsSATScores schoolsSATScores) {
        Intent intent = new Intent(context, SchoolsDetailActivity.class);
        intent.putExtra(EXTRA_SCHOOL, school);
        intent.putExtra(EXTRA_SCHOOL_SAT, schoolsSATScores);
        return intent;
    }

    private void displayHomeAsUpEnabled() {
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
        }
    }

    private void getExtrasFromIntent() {
        School school = (School) getIntent().getSerializableExtra(EXTRA_SCHOOL);
        SchoolsSATScores schoolsSATScores = (SchoolsSATScores) getIntent().getSerializableExtra(EXTRA_SCHOOL_SAT);
        SchoolsDetailViewModel schoolsDetailViewModel = new SchoolsDetailViewModel(school, schoolsSATScores);
        schoolsDetailActivityBinding.setSchoolDetailViewModel(schoolsDetailViewModel);
        setTitle(school.getSchoolName());
    }
}
